import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { NewsApiProvider } from '../../providers/news-api/news-api';

/**
 * Generated class for the NewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
})
export class NewsPage {
  public listArticles;
  public id_source:string;
  constructor(public navCtrl: NavController, public navParams: NavParams,public newsApi:NewsApiProvider) {
    this.listArticles=[];
    this.id_source=navParams.data.id;
    //console.log(navParams.data.id);
    this.newsApi.getTopNews(this.id_source).subscribe(response=>{
      this.listArticles=response['articles'];
      //console.log(response);
    });
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad NewsPage');
  }
  public goToPageHome(){
    this.navCtrl.setRoot(HomePage);
  }
}
