import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

/*
  Generated class for the NewsApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NewsApiProvider {
  keyApi="fa04fc1796644ae3af1eda6cd4f96755";
  constructor(public http: HttpClient) {
    console.log('Hello NewsApiProvider Provider');
    
  }

  getSources(){
    return this.http.get('https://newsapi.org/v2/sources?apiKey='+this.keyApi).map(res=>res);
  }

  getTopNews(id){
    return this.http.get('https://newsapi.org/v2/top-headlines?sources='+id+'&apiKey='+this.keyApi).map(res=>res);
  }

}
